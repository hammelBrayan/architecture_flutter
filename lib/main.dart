import 'package:flutter/material.dart';

import 'common/constant.dart';
import 'pages/splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Colis ',
      theme: ThemeData(
        fontFamily: 'DayRoman',
        scaffoldBackgroundColor: kBackgroundColor,
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        appBarTheme: const AppBarTheme(
            elevation: 0,
            backgroundColor: kBackgroundColor,
            actionsIconTheme: IconThemeData(
              color: Colors.black,
            )),
        primaryColor: kHeaderColor,
        textTheme: Theme.of(context)
            .textTheme
            .apply(bodyColor: kTeXTColor, fontFamily: 'DayRoman'),
        primarySwatch: Colors.red,
      ),
      home: const SplashScreen(),
    );
  }
}
